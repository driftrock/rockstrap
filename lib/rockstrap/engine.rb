module Rockstrap
  class Engine < ::Rails::Engine
    require 'bootstrap-sass'
    require 'autoprefixer-rails'
    isolate_namespace Rockstrap
    initializer "rockstrap-assets" do |app|
      app.config.assets.precompile += ['rockstrap.css', 'rockstrap.js']
    end
    initializer 'rockstrap-helpers' do |app|
      ActiveSupport.on_load :action_view do
        include Rockstrap::RockstrapHelper
      end
    end
  end
end
