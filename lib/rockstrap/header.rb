module Rockstrap
  class Header
    attr_reader :helper, :app_name, :header_builder
    def initialize(helper, app_name, &block)
      @helper = helper
      @app_name = app_name
      @header_builder = HeaderBuilder.new(@helper)
      block.call(@header_builder) if block_given?
    end

    def render
      helper.render partial: 'rockstrap/header', locals: {
        app_name: app_name,
        navigation_body: header_builder.navigation_body,
        apps_body: header_builder.apps_body
      }
    end
  end

  class HeaderBuilder
    attr_reader :helper, :navigation_body, :apps_body
    def initialize(helper)
      @helper = helper
      @navigation_body = helper.render(partial: 'rockstrap/header_navigation')
      @apps_body = nil
    end

    def navigation(&block)
      @navigation_body = helper.capture(&block)
    end

    def apps(&block)
      @apps_body = helper.capture(&block)
    end
  end
end
