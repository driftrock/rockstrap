require "rockstrap/engine"
require "rockstrap/header"

module Rockstrap
  def self.configuration
    @configuration ||= Configuration.new
  end

  def self.configure
    yield(configuration) if block_given?
  end

  class Configuration < OpenStruct; end
end
