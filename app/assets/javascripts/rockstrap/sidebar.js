var Sidebar = function($) {

  var siteWrapper = $('.site-wrapper'),
      sideNav = $('nav.sidebar-nav'),
      windowWidth = $(window).width();

  var self = {};

  self.buildUi = function() {
    if(windowWidth > 567) {
      if(sideNav.length > 0) {

        // check for touch device and disable first click in order to open side nav
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {

          sideNav.find('a:not(.close)').on('click', function(e) {
            if(siteWrapper.hasClass('show-nav')) {
              e.preventDefault();
              siteWrapper.removeClass('show-nav');
            }
          });

          // add close button to side nav
          sideNav.find('ul').append('<li class="close"><a href="#" style="margin-top: 40px;">Close side bar</a></li>');

          sideNav.find('.close').on('click', function(e) {
            e.preventDefault();
            siteWrapper.removeClass('show-nav');
          });
        }
        else {
          sideNav.on('mouseover', function() {
            siteWrapper.addClass('show-nav');
          }).on('mouseout', function() {
            siteWrapper.removeClass('show-nav');
          });
        }
      }
    } else {
      $(document).on('click', '.app-menu', function() {
        siteWrapper.toggleClass('show-nav');
      });
    }
  }

  return self;

}(jQuery);

$(function() {
  Sidebar.buildUi();
});