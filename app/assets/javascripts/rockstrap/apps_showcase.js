Rockstrap.AppsShowcase = (function(){
  var bindClicks, clearSelected;

  function appsShowcase(element, options) {
    this.$element = $(element);
    this.options = options;
    this.apps = this.options.apps;
    bindClicks(this);
  }

  bindClicks = function(instance) {
    var _this = instance,
        elements = $('.app a', _this.$element);

    elements.each(function(){
      $(this).on('click', function(event){
        var app = $(event.target).closest('.app'),
            appName = app.data('app-name');

        if ($.inArray(appName, _this.apps) > -1) {
          _this.openShowCaseFor(appName);
          return false;
        }
      });
    });
  }

  clearSelected = function(instance) {
    var elements = $('.app', instance.$element);
    elements.each(function(){
      $(this).removeClass('selected');
    });
  }

  appsShowcase.prototype.openShowCaseFor = function(appName){
    var showCaseDiv = $('.showcase', this.$element),
        appDiv = $('.app[data-app-name='+appName+']', this.$element);

    clearSelected(this);
    appDiv.addClass('selected');
    showCaseDiv.slideUp('fast', function(){
      $('.app-show-case', showCaseDiv).removeClass('active');
      $('.app-show-case.'+appName, showCaseDiv).addClass('active');
      showCaseDiv.slideDown('slow');
    });
  }

  return appsShowcase;
})();
