require_dependency "rockstrap/application_controller"

module Rockstrap
  class DemoController < ApplicationController
    def index
      require 'haml'
    end
  end
end
