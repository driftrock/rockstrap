module Rockstrap::RockstrapHelper

  # Shortcut to include Bootstrap icons
  # icon_tag('plus') => <span class="glyphicon glyphicon-plus"></span>

  def website_url
    Rockstrap.configuration.website_url
  end

  def icon_tag(icon_name)
    content_tag(:span, '', class: "glyphicon glyphicon-#{icon_name}").html_safe
  end

  def rockstrap_header(app_name = 'App Name', &block)
    Rockstrap::Header.new(self, app_name, &block).render
  end

  def rockstrap_static_header(title, &block)
    render partial: 'rockstrap/static_header', locals: { title: title, navigation: capture(&block) }
  end

  def rockstrap_plain_header(title, &block)
    render partial: 'rockstrap/plain_header', locals: { title: title, navigation: capture(&block) }
  end

  def rockstrap_beta_message(&block)
    render partial: 'rockstrap/beta_message', locals: { body: capture(&block) }
  end

  def rockstrap_typekit_fonts
    render partial: 'rockstrap/typekit_fonts'
  end

end
