$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "rockstrap/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "rockstrap"
  s.version     = Rockstrap::VERSION
  s.authors     = %w(Tiago Emma Daniel Max)
  s.email       = ["contact@driftrock.com"]
  s.homepage    = ""
  s.summary     = "Rockstrap styling for driftrock"
  s.description = ""
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]

  s.add_dependency "rails", ">= 4.0.0"
  s.add_dependency "bootstrap-sass"
  s.add_dependency "autoprefixer-rails"
  s.add_dependency "haml"
  s.add_dependency "sass"
  s.add_development_dependency "rspec-rails"
end
